package gitops.restapi.controllers;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InfoController {

    private final String version;
    private final String title;

    @Autowired
    public InfoController(
        @Value("${app.title}") String title,
        @Value("${app.version}") String version
    ) {
        this.version = Objects.requireNonNull(version);
        this.title = Objects.requireNonNull(title);
    }

    // curl -v localhost:8080/api/version
    @GetMapping("/api/version")
    public ResponseEntity<String> getVersion() {
        return ResponseEntity.ok(version);
    }

    // curl -v localhost:8080/api/info
    @GetMapping("/api/info")
    public ResponseEntity<String> getInfo() {
       return ResponseEntity.ok(String.format("%s - %s", title, version));
    }

}
