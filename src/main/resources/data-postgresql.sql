insert into "users"("id", "first_name", "last_name", "email", "age")
 values (1, 'Martin', 'Boßlet', 'martin.bosslet@gmail.com', 42);
insert into "users"("id", "first_name", "last_name", "email", "age")
 values (2, 'Max', 'Mustermann', 'max@example.org', 21);

create sequence "seq"
  START WITH 3
  INCREMENT BY 1;